#Código Sequencial sem uso de Threads
import requests #Importação da Biblioteca import
import time #Importando a biblioteca de time
import concurrent.futures #importando a biblioteca de Threds

url = "https://api.github.com/users/gabrielschade"
resposta = ""
def requisicao(url):
    inicio_time = time.time() #Dando inicio ao tempo para saber quanto tempo levou para fazer a requisição
    global resposta
    resposta = requests.get("https://api.github.com/users/gabrielschade") #Fazendo a requisição ao site
    fim_time = time.time() #Tempo finalizado
    tempo_decorrido = fim_time - inicio_time # Calcula o tempo
    
    return resposta.status_code, tempo_decorrido #Retorno dos Resultados

print()

requisicao(url)
with concurrent.futures.ThreadPoolExecutor(max_workers=5) as threads:
    results = threads.map(requisicao, url)

    for resultado in results:
        status_code, tempo_decorrido = resultado
print(f"O status do código foi: {status_code}, Tempo gasto foi: {tempo_decorrido:.3f} segundos")
print()
print(resposta.json())
print()
