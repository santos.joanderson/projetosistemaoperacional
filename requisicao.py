#Código Sequencial sem uso de Threads
import requests #Importação da Biblioteca import
import time


inicio_time = time.time() #Dando inicio ao tempo para saber quanto tempo levou para fazer a requisição
response = requests.get("https://api.github.com/users/gabrielschade") #Fazendo a requisição ao site
fim_time = time.time() #Tempo finalizado

tempo_decorrido = fim_time - inicio_time # Calcula o tempo

print()
print("Status do Código:",response.status_code)
print(response.json())
print()

print(f"Tempo gasto foi: {tempo_decorrido:.3f} segundos")
print()
